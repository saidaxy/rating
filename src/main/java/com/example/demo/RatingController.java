package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController

@RequestMapping("/rating")

public class RatingController {



    @GetMapping("/{roommateId}")

    public Rating getRatingByRoommateId(

            @PathVariable("roommateId") String roommateId) {



        return new Rating(roommateId, 5);

    }

}
